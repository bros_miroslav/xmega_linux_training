#ifndef USART_H
#define USART_H


#include <stdbool.h>
#include <stdint.h>
#include <avr/io.h>
 
#define LINE_SEPARATOR '\n'
#define likely(x)       __builtin_expect(!!(x),1)
#define unlikely(x)     __builtin_expect(!!(x),0)
 
typedef void (*uart_rx_callback_t)(const char *s);
 
typedef struct {
    volatile char *buffer; //must be null-terminated, shared by RX and TX directions
    uint8_t buffer_size;
    uint8_t buffer_index; //used wit uart_task
    uint8_t buffer_head;  //used with ringbuffer
    uint8_t buffer_tail;  //used with ringbuffer
    bool line_received;
    uart_rx_callback_t rx_callback;
    USART_t* usart;
    DMA_CH_t* dma_ch;
    uint8_t dma_trigger_source;
} uart_state_t;
 
/** Initializes the driver with a particular buffer and USART.
 * @param t Pointer to USART struct, eg. &USARTD0
 * @param dma_ch Pointer to DMA channel struct, eg. &DMA.CH0, NULL if DMA is not needed.
 * @param s Pointer to uart_state_t struct, allocated statically.
 * @param rx_callback Pointer to function that should be called whenever a complete line is received
 * @param buffer Pointer to array that will be used as the TX and RX buffer
 * @param buffer_size Size of buffer array.
 */
void uart_init(USART_t* t, DMA_CH_t* dma_ch, volatile uart_state_t* s, uart_rx_callback_t rx_callback, volatile char *buffer, uint8_t buffer_size);
 
/** Line received callbacks are delivered from this function. It should be called
 * from main loop. Not used with RX ringbuffer!
 * @param t Pointer to uart_state_t struct, allocated statically.
 */
void uart_task(volatile uart_state_t* t);
 
/** This function has to be called to obtain the buffer to fill with
 * data to be transmitted. It also stops RX operation of the driver.
 * DATA IN BUFFER MUST BE NULL TERMINATED.
 * After filling the buffer uart_send has to be called.
 * @param s Pointer to uart_state_t struct, allocated statically.
 * @return Pointer to buffer to be filled with data. The same as buffer argument of uart_init.
 */
char* uart_get_buffer(volatile uart_state_t *s);
 
/** Starts sending data in buffer (from uart_get_buffer)
 * @param s Pointer to uart_state_t struct, allocated statically.
 */
void uart_send(volatile uart_state_t* s);
 
/** Gets a single byte from receive ringbuffer.
 * @param t Pointer to uart_state_t struct, allocated statically.
 * @partam target Pointer where the byte from ringbuffer is to be put.
 * @return true if a byte was received, false otherwise
 */
bool uart_getc(volatile uart_state_t* t, char *target);
 
/** Receive interrupt handler in line mode. To be called within ISR(USARTxn_RXC_vect) function.
 * @param t Pointer to uart_state_t struct, allocated statically.
 */
void uart_rx_isr(volatile uart_state_t* t);
 
/** Receive interrupt handler in ringbuffer mode. To be called within ISR(USARTxn_RXC_vect) function.
 * @param t Pointer to uart_state_t struct, allocated statically.
 */
void uart_rx_isr_ringbuffer(volatile uart_state_t* t);
 
/** Transmit interrupt handler. Used when no DMA channel
 * is specified. To be called within ISR(USARTxn_DRE_vect) function.
 * @param t Pointer to uart_state_t struct, allocated statically.
 */
void uart_dre_isr(volatile uart_state_t* t);
 
/** Transmit interrupt handler. Used with a DMA channel
 * interrupt. To be called within ISR(DMA_CHx_vect) function.
 * @param t Pointer to uart_state_t struct, allocated statically.
 */
void uart_dma_isr(volatile uart_state_t* t);

#endif