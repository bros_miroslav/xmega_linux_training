/*
* Author: M0AGX
* 
* Link: https://m0agx.eu/2017/04/02/xmega-usart-driver-with-tx-dma/
**/

#include <string.h>
#include <util/delay.h>
#include "usart/includes/usart.h"
 
/* --------- private prototypes --------- */
static void uart_end_transmission(volatile uart_state_t *t);
 
/* ----------- implementation ----------- */
 
void uart_init(USART_t* u,
               DMA_CH_t *dma_ch,
               volatile uart_state_t* s,
               uart_rx_callback_t rx_callback,
               volatile char *buffer,
               uint8_t buffer_size){
 
    u->CTRLB = USART_TXEN_bm | USART_RXEN_bm; //enable receiver and transmitter
 
    //baud setting
    u->BAUDCTRLA = 5;                   //BSEL   = 5  115200bps
    u->BAUDCTRLB = 0; //BSCALE =  3 @11.059200MHz
 
    s->buffer = buffer;
    s->buffer_size = buffer_size;
    s->rx_callback = rx_callback;
    s->usart = u;
    s->line_received = false;
    s->buffer_index = 0;
    s->buffer_head = 0;
    s->buffer_tail = 0;
 
    s->dma_ch = dma_ch;
    if (dma_ch){ //look for trigger source
        switch ((uint16_t)u){ //pointer has to be cast the ugly way 
        #ifdef USARTC0
            case (uint16_t)&USARTC0:
                s->dma_trigger_source = DMA_CH_TRIGSRC_USARTC0_DRE_gc; break;
        #endif
        #ifdef USARTC1
            case (uint16_t)&USARTC1:
                s->dma_trigger_source = DMA_CH_TRIGSRC_USARTC1_DRE_gc; break;
        #endif
        #ifdef USARTD0
            case (uint16_t)&USARTD0:
                s->dma_trigger_source = DMA_CH_TRIGSRC_USARTD0_DRE_gc; break;
        #endif
        #ifdef USARTD1
            case (uint16_t)&USARTD1:
                s->dma_trigger_source = DMA_CH_TRIGSRC_USARTD1_DRE_gc; break;
        #endif
        #ifdef USARTE0
            case (uint16_t)&USARTE0:
                s->dma_trigger_source = DMA_CH_TRIGSRC_USARTE0_DRE_gc; break;
        #endif
        #ifdef USARTF0
            case (uint16_t)&USARTF0:
                 s->dma_trigger_source = DMA_CH_TRIGSRC_USARTF0_DRE_gc; break;
        #endif
        }
    }
 
    //enable RXC interrupt - low priority
    //DRE interrupt must not be enabled, otherwise it will fire immediately
    u->CTRLA = USART_RXCINTLVL_LO_gc;
}
 
void uart_task(volatile uart_state_t *t){
    if (t->line_received){
        t->line_received = false;
        if (t->rx_callback){
            t->buffer_index = 0; //MUST BE BEFORE CALLBACK!
            t->rx_callback((const char*)t->buffer);
        }
    }
}
 
bool uart_getc(volatile uart_state_t* t, char *target){
    if (t->buffer_tail != t->buffer_head){
        uint8_t current_head = t->buffer_head;
        t->buffer_head = (t->buffer_head + 1) % t->buffer_size; //UART_BUFFER_SIZE;
        *target = t->buffer[current_head];
        return true;
    }
    return false;
}
 
//this function transmits data that is already in the buffer
void uart_send(volatile uart_state_t* s){
    s->buffer[s->buffer_size-1] = '\0'; //enforce that the string is always null-terminated
    if (s->dma_ch){
        //globally enable DMA, no double buffering, round-robin
        DMA.CTRL  = DMA_ENABLE_bm;
 
        /* ------ TX DMA channel setup ------ */
        s->dma_ch->CTRLA = DMA_CH_RESET_bm;
        s->dma_ch->ADDRCTRL = DMA_CH_SRCDIR_INC_gc | DMA_CH_DESTDIR_FIXED_gc | DMA_CH_DESTRELOAD_TRANSACTION_gc; //source - increment, destination (USARTxx.DATA) fixed
        s->dma_ch->TRIGSRC = s->dma_trigger_source;
        s->dma_ch->DESTADDR0 = (((uint16_t)&(s->usart->DATA)) >> 0) & 0xFF;
        s->dma_ch->DESTADDR1 = (((uint16_t)&(s->usart->DATA)) >> 8) & 0xFF;
        s->dma_ch->DESTADDR2 = 0x00;
        s->dma_ch->SRCADDR0 = (((uint16_t)s->buffer) >> 0) & 0xFF;
        s->dma_ch->SRCADDR1 = (((uint16_t)s->buffer) >> 8) & 0xFF;
        s->dma_ch->SRCADDR2 = 0x00; //internal SRAM 
        s->dma_ch->TRFCNT = strlen((char*)s->buffer); //transfer length
 
        //enable channel, USART DRE will trigger the transmission, single shot mode - one byte per trigger
        s->dma_ch->CTRLA = DMA_ENABLE_bm | DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc; 
        s->dma_ch->CTRLB = DMA_CH_TRNINTLVL_LO_gc; //transfer complete interrupt enable
        /* ---------------------------------- */
    } else { //DMA channel is not set, use ISR
        s->usart->DATA = s->buffer[0]; //transmit first character
        s->buffer_index = 1;           //ISRs will take care of the rest
        s->usart->CTRLA = USART_DREINTLVL_LO_gc; //enable DRE interrupt, disable RX interrupt
    }
}
 
char* uart_get_buffer(volatile uart_state_t *s){
    s->usart->CTRLA = 0; //disable all interrupts (stop receiving)
    s->buffer_tail = 0;
    s->buffer_head = 0;
    return (char*)s->buffer;
}
 
inline void uart_rx_isr(volatile uart_state_t *t){
    t->buffer[t->buffer_index] = t->usart->DATA;
    if (unlikely(t->buffer_index >= t->buffer_size-1)){ //rx buffer overflow protection
        t->buffer_index = 0;
    }
    if (unlikely(t->buffer[t->buffer_index] == LINE_SEPARATOR)){ //we received newline, let's terminate the command
        t->buffer_index++;
        t->buffer[t->buffer_index] = '\0';
        t->line_received = true; //now the line will be processed by uart_task
        t->buffer_index = 0; //hack
    } else {
        t->buffer_index++;
    }
}
 
inline void uart_rx_isr_ringbuffer(volatile uart_state_t *t){
    uint8_t next = (t->buffer_tail + 1) % t->buffer_size;
    t->buffer[next] = t->usart->DATA;
    t->buffer_tail = next;
}
 
inline void uart_dre_isr(volatile uart_state_t *t){ //data register ready for more data interrupt
    if (t->buffer[t->buffer_index]){ //if we have more data to transmit (string must be null-terminated)
        t->usart->DATA = t->buffer[t->buffer_index];
        t->buffer_index++;
    } else { //no more data to transmit
        uart_end_transmission(t);
    }
}
 
inline void uart_dma_isr(volatile uart_state_t* t){
    t->dma_ch->CTRLA = 0; //disable channel
    t->dma_ch->CTRLB = 0; //disable channel interrupts
    uart_end_transmission(t);
    //PORTF.OUTTGL = PIN6_bm;
}
 
inline static void uart_end_transmission(volatile uart_state_t *t){
    //prepare buffer for reception
    t->buffer_index = 0;
    t->buffer_tail = 0;
    t->buffer_head = 0;
    t->usart->CTRLA = USART_RXCINTLVL_LO_gc; //disable DRE interrupt, enable RX interrupt
    //PORTF.OUTTGL = (PIN6_bm);
}