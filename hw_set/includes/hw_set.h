#ifndef HW_SET_H
#define HW_SET_H

#include <avr/io.h>
#include <avr/interrupt.h>

void hw_set_initial();
void hw_set_clock();
void hw_port_pin_set();

#endif