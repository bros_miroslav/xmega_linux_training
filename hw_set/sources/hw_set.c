#include "hw_set/includes/hw_set.h"

void hw_set_initial()
{
    int n;
    // Interrupt system initialization
    // Optimize for speed
    //#pragma optsize-7
    // Make sure the interrupts are disabled
    cli();
    //asm("cli");
    // Low level interrupt: On
    // Round-robin scheduling for low level interrupt: Off
    // Medium level interrupt: On
    // High level interrupt: On
    // The interrupt vectors will be placed at the start of the Application FLASH section
    n=(PMIC.CTRL & (~(PMIC_RREN_bm | PMIC_IVSEL_bm | PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm))) |
	PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    CCP=CCP_IOREG_gc;
    PMIC.CTRL=n;
    // Set the default priority for round-robin scheduling
    PMIC.INTPRI=0x00;
}

void hw_set_clock()
{
    OSC.XOSCCTRL = OSC_FRQRANGE_9TO12_gc | OSC_XOSCSEL_XTAL_16KCLK_gc; /* Konfiguruj XTAL vstup */
    OSC.CTRL |= OSC_XOSCEN_bm;                                          /* start XTAL */
    while (!(OSC.STATUS & OSC_XOSCRDY_bm));                             /* wait until ready */
	OSC.CTRL |= OSC_XOSCEN_bm;                                          /* povolit externy krystal */
    while (!(OSC.STATUS & OSC_XOSCRDY_bm));                             /* wait until ready */
	CCP = CCP_IOREG_gc;                                                 /* allow changing CLK.CTRL */
    CLK.CTRL = CLK_SCLKSEL_XOSC_gc; 
}

void hw_port_pin_set()
{
    /******************PORTF******************/
    /* port directions*/
    /* LEDs */
    PORTF.DIR = PIN6_bm | PIN7_bm;
    
    /* pin initial values*/
    /* LEDs */
    PORTF.OUTSET = PIN6_bm;
    PORTF.OUTCLR = PIN7_bm;
    /*****************************************/

    /******************PORTE******************/
    /* port directions */
    /* USARTE0 UART */
    PORTE.DIRSET = PIN3_bm; 
    PORTE.DIRCLR = PIN2_bm;
    /*****************************************/

    /******************PORTA******************/
    /* port directions */
    /* VCC to A0 for UART */
    PORTA.DIRSET = PIN0_bm;
    PORTA.OUTSET = PIN0_bm;
    /*****************************************/
}
