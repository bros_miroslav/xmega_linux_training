#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdio.h>
#include <util/delay.h>


#include "serial_logger/serial_logger.h"
#include "usart/includes/usart.h"
#define CRLF "\r\n"
#define SERIAL_LOG_BUFFER_FMT "%sCRLF"
 
/* --------- private data --------------- */
static volatile char _uart_buffer[250];
static uart_state_t _logger_uart_state;
static char _logger_line_buffer[100];
static uint8_t _logger_line_buffer_index = 0;

void serial_logger_init(void)
{
    uart_init(&USARTE0, &DMA.CH0, &_logger_uart_state, NULL /*line callback*/,
	_uart_buffer, sizeof(_uart_buffer));
}

void log_message(const char *message)
{
    char* buffer = uart_get_buffer(&_logger_uart_state);
    strcpy(buffer, message);
    uart_send(&_logger_uart_state);
}

void gps_uart_task(void){
    char c = 0;
    char message[50];
    while (uart_getc(&_logger_uart_state, &c))
    {
	if (c == 'X')
	{ //whole line has arrived - fire callback
            _logger_line_buffer[_logger_line_buffer_index++] = c; //add the newline, otherwise parser will fail
            _logger_line_buffer[_logger_line_buffer_index] = '\0';
            //TODO: process the line
           sprintf(message, "%s\r\n", &_logger_line_buffer[0]);
	   log_message((const char*)message);

	    _logger_line_buffer_index = 0;
            break;
        }

	_logger_line_buffer[_logger_line_buffer_index] = c;
        _logger_line_buffer_index++;
	
        if (unlikely(_logger_line_buffer_index > sizeof(_logger_line_buffer)-1))
	{
            _logger_line_buffer_index = 0;
        }
    }
}

/* communication over DMA or INTERUPTs */
ISR(DMA_CH0_vect){
    uart_dma_isr(&_logger_uart_state);
}

/*
ISR(USARTE0_DRE_vect){
    uart_dre_isr(&_logger_uart_state);
}
*/
ISR(USARTE0_RXC_vect){
    uart_rx_isr_ringbuffer(&_logger_uart_state);
}
