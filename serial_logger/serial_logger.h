#ifndef SERIAL_LOGGER_H
#define SERIAL_LOGGER_H

#define USART_DMA 1
#define INIT_LOG_MESSAGE "\r\n*********************************\r\n\
Author: Miroslav Bros\r\n\
Date: 01.11.2021\r\n\
Demo program for Vlado board\r\n\
*********************************\r\n\r\n"

void serial_logger_init(void);
void gps_uart_task(void);
void log_message(const char *message);


#endif