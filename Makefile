MCU=atxmega128a3u
F_CPU=11059200
PROGRAMMER=avrispmkII
FLASH_CPU=x128a3u 
CC=avr-gcc
OBJCOPY=avr-objcopy
CFLAGS=-std=c99 -Wall -g -Os -mmcu=${MCU} -DF_CPU=${F_CPU} -I.
TARGET=main
SRCS=main.c hw_set/sources/hw_set.c usart/sources/usart.c serial_logger/serial_logger.c \
FreeRTOS/Source/event_groups.c FreeRTOS/Source/heap_1.c \
FreeRTOS/Source/list.c FreeRTOS/Source/port.c FreeRTOS/Source/queue.c \
FreeRTOS/Source/tasks.c FreeRTOS/Source/timers.c FreeRTOS/Source/croutine.c FreeRTOS/Source/stream_buffer.c

all:
	${CC} ${CFLAGS} -o ${TARGET}.bin ${SRCS}
	${OBJCOPY} -j .text -j .data -O ihex ${TARGET}.bin ${TARGET}.hex
	avr-size -C ${TARGET}.bin
	@echo "...compile and hex done..."

flash:
	avrdude -F -V -c ${PROGRAMMER} -p ${FLASH_CPU} -P usb -U flash:w:main.hex
	@echo "...flash done..."
clean:
	rm -f *.bin *.hex
	@echo "...clean done..."
